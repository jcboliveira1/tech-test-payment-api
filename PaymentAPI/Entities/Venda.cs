using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestPaymentAPI.Entities
{
    public class Venda
    {
        [Key()]
        public int Id { get; set; }        
        public Vendedor Vendedor { get; set; }            
        public List<ItemVenda> ItensDaVenda { get; set; }
        public DateTime DataVenda { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}