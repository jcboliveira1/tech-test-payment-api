using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestPaymentAPI.Entities;
using TestPaymentAPI.Context;
using Microsoft.EntityFrameworkCore;

namespace TestPaymentAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly TestPaymentAPIContext _context;

        public VendaController(TestPaymentAPIContext context)
        {
            _context = context;
        }

        [HttpPost("IncluirVenda")]
        public IActionResult IncluirVenda(List<ItemVenda>  Produtos, int Vendedor)
        {
            
            var vendedorOperacao = _context.Vendedores.Find(Vendedor);

            if(vendedorOperacao == null){
                return BadRequest(new { Erro = "Vendedor não cadastrado ou não localizado." });
            }
            
            if (Produtos == null)
                return BadRequest("Sem produtos para registrar a venda.");

            if (Produtos.Count() == 0)
                return BadRequest("É necessário enviar pelo menos um (01) produto para gerar uma venda.");

            Venda novaVenda = new Venda();

            novaVenda.Status = EnumStatusVenda.AguardandoPagamento;
            novaVenda.DataVenda = DateTime.Now;
            novaVenda.Vendedor = vendedorOperacao;
            novaVenda.ItensDaVenda = Produtos;
            
            _context.Add(novaVenda);
            _context.SaveChanges();
            
            return CreatedAtAction(nameof(BuscarVendaPorId), new { id = novaVenda.Id }, novaVenda);  
        }

        [HttpGet("BuscarVendaPorId")]
        public IActionResult BuscarVendaPorId(int id)
        {
            var vendaAtual = _context.Vendas.Find(id);

            if (vendaAtual == null)
                return NotFound();

            var vendaRetorno = _context.Vendas.Where(x => x.Id == id)
                              .Include(v => v.Vendedor)
                              .Include(i => i.ItensDaVenda);

            return Ok(vendaRetorno);
        }
        
        [HttpPost("AtualizarStatusVenda")]
        public IActionResult AtualizarStatusVenda(int id, EnumStatusVenda statusEnviado)
        { 
            var vendaAtual = _context.Vendas.Find(id);

             if (vendaAtual == null)
                return NotFound();

            EnumStatusVenda novoStatusVenda;

            switch (vendaAtual.Status)
            {
                case EnumStatusVenda.AguardandoPagamento:
                    
                    if ((statusEnviado == EnumStatusVenda.PagamentoAprovado) || (statusEnviado == EnumStatusVenda.Cancelado)){
                        novoStatusVenda = statusEnviado; 
                    } else {
                        return BadRequest(new { Erro = "Atualização não disponivel ou não permitida!" });  
                    }

                    break;
                case EnumStatusVenda.PagamentoAprovado:
                    
                    if ((statusEnviado == EnumStatusVenda.EnviadoParaTransportadora) || (statusEnviado == EnumStatusVenda.Cancelado)){
                        novoStatusVenda = statusEnviado; 
                    } else {
                        return BadRequest(new { Erro = "Atualização não disponivel ou não permitida!" });  
                    }

                    break;
                case EnumStatusVenda.EnviadoParaTransportadora:
                    
                    if (statusEnviado == EnumStatusVenda.Entregue){
                        novoStatusVenda = statusEnviado; 
                    } else {
                        return BadRequest(new { Erro = "Atualização não disponivel ou não permitida!" });  
                    }

                    break;
                default:
                    return BadRequest(new { Erro = "Atualização não disponivel ou não permitida!" });
                    break;
            }

            vendaAtual.Status = novoStatusVenda;

            _context.Vendas.Update(vendaAtual);
            _context.SaveChanges();

            return Ok(new { Status = "Atualização realizada com sucesso!"});  
        }
    }
}