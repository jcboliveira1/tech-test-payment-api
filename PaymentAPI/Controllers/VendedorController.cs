using Microsoft.AspNetCore.Mvc;
using TestPaymentAPI.Entities;
using TestPaymentAPI.Context;

namespace TestePaymentAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly TestPaymentAPIContext _context;
        public VendedorController(TestPaymentAPIContext context)
        {
            _context = context;
        }

        [HttpPost("IncluirVendedor")]
        public IActionResult Incluir(Vendedor novoVendedor)
        {
            _context.Add(novoVendedor);
            _context.SaveChanges();
            
            return CreatedAtAction(nameof(BuscarVendedorPorId), new { id = novoVendedor.Id }, novoVendedor);  
        }
        [HttpGet("BuscarVendedorPorId")]
        public IActionResult BuscarVendedorPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if(vendedor == null){
                return NotFound();
            }

            return Ok(vendedor);
        }
        [HttpGet("BuscarTodosVendedores")]
        public IActionResult BuscarTodosVendedores()
        {
            var vendedor = _context.Vendedores;
            return Ok(vendedor);
        }
        [HttpDelete("ExcluirVendedor")]
        public IActionResult Excluir(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if (vendedor == null){
                return NotFound();
            }
            _context.Vendedores.Remove(vendedor);
            _context.SaveChanges();
            return NoContent();
        }
        [HttpPut("AtualizarVendedor")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorAtual = _context.Vendedores.Find(id);
            
            if (vendedorAtual == null){
                return NotFound();
            }

            vendedorAtual.Nome = vendedor.Nome;
            vendedorAtual.Cpf = vendedor.Cpf;
            vendedorAtual.Email = vendedor.Email;
            vendedorAtual.Telefone = vendedor.Telefone;

            _context.Vendedores.Update(vendedorAtual);
            _context.SaveChanges();
            return Ok(vendedorAtual);
        }

    }
}