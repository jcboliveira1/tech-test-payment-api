using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestPaymentAPI.Entities;

namespace TestPaymentAPI.Context
{
    public class TestPaymentAPIContext : DbContext
    {
        public TestPaymentAPIContext(DbContextOptions<TestPaymentAPIContext> options) : base(options)
        {

        }
        
        public DbSet<Venda> Vendas {get; set; }
        public DbSet<Vendedor> Vendedores {get; set; }

    }
}