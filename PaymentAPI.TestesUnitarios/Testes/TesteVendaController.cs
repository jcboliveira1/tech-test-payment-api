using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestPaymentAPI.Context;
using TestPaymentAPI.Controllers;
using TestPaymentAPI.Entities;

using PaymentAPI.TestesUnitarios.RepositorioTeste;

namespace PaymentAPI.TestesUnitarios.Testes
{
    public class TesteVendaController
    {
        private readonly TestPaymentAPIContext _context;
        VendaController _vendaController;

        public TesteVendaController()
        {

            var options = new DbContextOptionsBuilder<TestPaymentAPIContext>()
                .UseInMemoryDatabase(databaseName: "dbVendasTesteUnitario")
                .Options;

            _context = new TestPaymentAPIContext(options);
            _vendaController = new VendaController(_context);

            // Incluindo dois vendedores
            _context.Vendedores.Add(new VendedorRepositorioTeste().Vendedor01());
            _context.Vendedores.Add(new VendedorRepositorioTeste().Vendedor02());

            // Incluido duas vendas
            _context.Vendas.Add(new VendasRepositorioTeste().TesteVendas01());
            _context.Vendas.Add(new VendasRepositorioTeste().TesteVendas02());   
            _context.Vendas.Add(new VendasRepositorioTeste().TesteVendas02());           
            _context.SaveChanges();
        }

        // Testes:
        [Theory(DisplayName = "Teste 01: Incluir venda Ok")]
        [MemberData(nameof(DbProdutosTeste))]
        public void IncluirVendaTeste(int IdVendedor, DateTime Data, List<ItemVenda> ListaProdutos)
        {
            var resultado = _vendaController.IncluirVenda(ListaProdutos, IdVendedor);
            Assert.IsAssignableFrom<CreatedAtActionResult>(resultado);
        }
        
        [Fact(DisplayName = "Teste 02: Incluir venda com vendedor não cadastrado")]
        public void IncluirVendaSemVendedorTeste()
        {
            var resultado = _vendaController.IncluirVenda(new ItensRepositorioTeste().VendaSimulada03(), 301);
            Assert.IsAssignableFrom<BadRequestObjectResult>(resultado);
        }

        [Fact(DisplayName = "Teste 03: Incluir venda sem produto")]
        public void IncluirVendaSemProdutoTeste()
        {
            var resultado = _vendaController.IncluirVenda(null, 1);
            Assert.IsAssignableFrom<BadRequestObjectResult>(resultado);
        }


        [Fact(DisplayName = "Teste 04: Consultar venda por Id")]
        public void BuscarVendaPorIdTeste()
        {
            var resultado = _vendaController.BuscarVendaPorId(1);
            Assert.IsAssignableFrom<OkObjectResult>(resultado);          
        }

        [Fact(DisplayName = "Teste 05: Atualizar vários status da venda")]
        public void AtualizarStatusTeste()
        {
            _vendaController.AtualizarStatusVenda(1, EnumStatusVenda.PagamentoAprovado);
            _vendaController.AtualizarStatusVenda(1, EnumStatusVenda.EnviadoParaTransportadora);
            var resultado = _vendaController.AtualizarStatusVenda(1, EnumStatusVenda.Entregue);
            Assert.IsAssignableFrom<OkObjectResult>(resultado);
        }

        [Fact(DisplayName = "Teste 06: Atualizar status para Cancelado")]
        public void AtualizarStatusCanceladoTeste()
        {
            _vendaController.AtualizarStatusVenda(2, EnumStatusVenda.PagamentoAprovado);
            var resultado = _vendaController.AtualizarStatusVenda(2, EnumStatusVenda.Cancelado);
            Assert.IsAssignableFrom<OkObjectResult>(resultado);
        }

        [Theory(DisplayName = "Teste 07: Atualizar status com falha na regra de negócio")]
        [InlineData(EnumStatusVenda.EnviadoParaTransportadora)]
        [InlineData(EnumStatusVenda.Entregue)]
        public void AtualizarStatusFalhaTeste(EnumStatusVenda status)
        {
            _vendaController.AtualizarStatusVenda(3, status);
            var resultado = _vendaController.AtualizarStatusVenda(3, status);
            Assert.IsAssignableFrom<BadRequestObjectResult>(resultado);
        }

        public static IEnumerable<object[]> DbProdutosTeste()
        {
            yield return new object[]
            {
                1,
                new DateTime(2022, 11, 12),
                new ItensRepositorioTeste().VendaSimulada01()
            };
            yield return new object[]
            {
                2,
                new DateTime(2022, 11, 10),
                new ItensRepositorioTeste().VendaSimulada02()
            };
            yield return new object[]
            {
                1,
                new DateTime(2022, 11, 11),
                new ItensRepositorioTeste().VendaSimulada03()
            };
            yield return new object[]
            {
                2,
                new DateTime(2022, 11, 12),
                new ItensRepositorioTeste().VendaSimulada01()
            };

        }
    }

        
}