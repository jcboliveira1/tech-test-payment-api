﻿using TestPaymentAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentAPI.TestesUnitarios.RepositorioTeste
{
    internal class VendedorRepositorioTeste
    {
        public Vendedor Vendedor01()
        {
            return new Vendedor
            {
                Id = 0,
                Nome = "Vendedor 01",
                Cpf = "111.111.111-11",
                Email = "vend01@teste.com",
                Telefone = "(47) 1741-2111"
            };               
        }

        public Vendedor Vendedor02()
        {
            return new Vendedor
            {
                Id = 0,
                Nome = "Vendedor 02",
                Cpf = "222.222.222-22",
                Email = "vend02@teste.com",
                Telefone = "(47) 3254-7410"
            };
        }

         public Vendedor Vendedor03()
        {
            return new Vendedor
            {
                Id = 0,
                Nome = "Vendedor 03",
                Cpf = "333.333.333-33",
                Email = "vend03@yesye.com",
                Telefone = "(47) 9854-7412"
            };
        }       
    }
}
