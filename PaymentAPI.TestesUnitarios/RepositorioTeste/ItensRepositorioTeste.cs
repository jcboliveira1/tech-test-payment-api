﻿using TestPaymentAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentAPI.TestesUnitarios.RepositorioTeste
{
    internal class ItensRepositorioTeste
    {
        public List<ItemVenda> VendaSimulada01()
        {
            return new List<ItemVenda>
            {
                new ItemVenda
                {
                    Id = 0,
                    Descricao = "Geladeira LG",
                    Quantidade = 1,
                    Valor = 5098.99
                },
                new ItemVenda
                {
                    Id = 0,
                    Descricao = "Mix processador Arno",
                    Quantidade = 1,
                    Valor = 970.12
                },
                new ItemVenda
                {
                    Id = 0,
                    Descricao = "Ar condicionado Split",
                    Quantidade = 1,
                    Valor = 3200.95
                }
            };
        }

        public List<ItemVenda> VendaSimulada02()
        {
            return new List<ItemVenda>
            {
                new ItemVenda
                {
                    Id = 0,
                    Descricao = "Secador de cabelo",
                    Quantidade = 1,
                    Valor = 200.99
                }
            };
        }

        public List<ItemVenda> VendaSimulada03()
        {
            return new List<ItemVenda>
            {
                new ItemVenda
                {
                    Id = 0,
                    Descricao = "Panela inox",
                    Quantidade = 1,
                    Valor = 289.99
                },
                new ItemVenda
                {
                    Id = 0,
                    Descricao = "Talheres de prata",
                    Quantidade = 1,
                    Valor = 10352.97
                }
            };
        }
    }
}
