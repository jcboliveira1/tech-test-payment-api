﻿using TestPaymentAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentAPI.TestesUnitarios.RepositorioTeste
{
    internal class VendasRepositorioTeste
    {
        public Venda TesteVendas01()
        {
            return new Venda
            {
                Id = 0,
                DataVenda = new DateTime(2023, 1, 24, 22, 01, 00),
                ItensDaVenda = new ItensRepositorioTeste().VendaSimulada01(),
                Status = EnumStatusVenda.AguardandoPagamento,
                Vendedor = new VendedorRepositorioTeste().Vendedor01()
            };
        }

        public Venda TesteVendas02()
        {
            return new Venda
            {
                Id = 0,
                DataVenda = new DateTime(2023, 1, 24, 22, 11, 00),
                ItensDaVenda = new ItensRepositorioTeste().VendaSimulada01(),
                Status = EnumStatusVenda.AguardandoPagamento,
                Vendedor = new VendedorRepositorioTeste().Vendedor02()
            };
        }
    }
}
